package grifts

import (
	"polymath-solutions/grumpy/actions"

	"github.com/gobuffalo/buffalo"
)

func init() {
	buffalo.Grifts(actions.App())
}
