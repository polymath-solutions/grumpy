package main

import (
	"log"

	"polymath-solutions/grumpy/actions"
)

func main() {
	app := actions.App()
	if err := app.Serve(); err != nil {
		log.Fatal(err)
	}
}
